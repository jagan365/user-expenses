--------To Register a new user----------
URL: http://104.236.111.96/toptal/index.php/api/auth/register
Type: POST
Headers:-
	Content-Type: application/x-www-form-urlencoded
Fields:-
	name:example
	email:example@gmail.com
	password:example@1234


--------To Login----------
URL: http://104.236.111.96/toptal/index.php/api/auth/login
Type: POST
Headers:-
	Content-Type: application/x-www-form-urlencoded
Fields:-
	email:admin@test.com
	password:admin@123


--------To Logout----------
URL: http://104.236.111.96/toptal/index.php/api/auth/logout
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER/USER-----To view their profile info
URL: http://104.236.111.96/toptal/index.php/api/example/user
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded

	
ADMIN/MANAGER-----To view all users profile info
URL: http://104.236.111.96/toptal/index.php/api/example/user/all
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER-----To view specific user profile info
URL: http://104.236.111.96/toptal/index.php/api/example/user/<user id>
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER-----To create a new user
URL: http://104.236.111.96/toptal/index.php/api/example/user/
Type: POST
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:-
	name:example
	email:example@gmail.com
	password:example@1234
	role:user|manager|admin
	
	
ADMIN/MANAGER-----To update specific user profile info
URL: http://104.236.111.96/toptal/index.php/api/example/user/<user id>
Type: PUT
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:-
	name:example
	role:user|manager|admin
	
	
ADMIN/MANAGER-----To delete specific user profile info
URL: http://104.236.111.96/toptal/index.php/api/example/user/<user id>
Type: DELETE
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER/USER-----To view their expenses info
URL: http://104.236.111.96/toptal/index.php/api/example/expenses
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER/USER-----To view their specific expense info
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN-----To view specific user expenses info
URL: http://104.236.111.96/toptal/index.php/api/example/expenses?user_id=<user id>
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	

ADMIN-----To view specific user specific expense info
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>?user_id=<user id>
Type: GET
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER/USER-----To create their own expenses
URL: http://104.236.111.96/toptal/index.php/api/example/expenses
Type: POST
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:-
	description:**********
	date:2018-04-03
	time:12:00:00
	amount:1000.00
	comment:**********
	
	
ADMIN/MANAGER/USER-----To create expenses for a specific user
URL: http://104.236.111.96/toptal/index.php/api/example/expenses?user_id=<user id>
Type: POST
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:-
	description:**********
	date:2018-04-03
	time:12:00:00
	amount:1000.00
	comment:**********
	
	
ADMIN/MANAGER/USER-----To update their own expenses
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>
Type: PUT
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:- (provide atleast 1 field to update)
	description:**********
	date:2018-04-03
	time:12:00:00
	amount:1000.00
	comment:**********
	
	
ADMIN-----To update a specific user expenses
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>
Type: PUT
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
Fields:- (provide atleast 1 field to update)
	description:**********
	date:2018-04-03
	time:12:00:00
	amount:1000.00
	comment:**********
	
	
ADMIN/MANAGER/USER-----To delete their own expenses
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>
Type: DELETE
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded
	
	
ADMIN/MANAGER/USER-----To delete a specific user expenses
URL: http://104.236.111.96/toptal/index.php/api/example/expenses/<expense id>
Type: DELETE
Headers:-
	token:**************(received at the time of login.)
	Content-Type: application/x-www-form-urlencoded