<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class expenses extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        //load database library
        $this->load->database();
    }

    /*
     * Fetch expenses data
     */
    /*public function getRows($id = "",$user_id = ""){
        if(!empty($id) && !empty($user_id)){
            $query = $this->db->get_where('expenses', array('id' => $id, 'user_id' => $user_id));
            return $query->row_array();
        }else{
            $query = $this->db->get_where('expenses', array('user_id' => $user_id));
            return $query->result_array();
        }
    }*/

    public function getRows($where){
        if(!empty($where) ){
            $this->db->where($where);
            $query = $this->db->get('expenses');
            return $query->result_array();
        }else{
            $query = $this->db->get('expenses');
            return $query->result_array();
        }
    }

    public function getSumAvg($where, $days){
        if(!empty($where) && !empty($days)){
            $this->db->select('sum(amount) as sum, sum(amount)/'.$days.' as average');
            $this->db->where($where);
            $query = $this->db->get('expenses');
            return $query->row_array();
        }else{
            return FALSE;
        }
    }

    /*
     * Fetch filter expenses data
     */
    public function getFilterRows($filter){
        if( !empty($filter) ) {
            $this->db->where($filter);
            $query = $this->db->get('expenses');
            return $query->result_array();
        } else{
            return FALSE;
        }
    }
    
    /*
     * Insert expenses data
     */
    public function insert($data = array()) {
        if(!array_key_exists('created_at', $data)) {
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists('modified_at', $data)){
            $data['modified_at'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert('expenses', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }
    
    /*
     * Update expenses data
     */
    public function update($data, $where) {
        if( !empty($where) && !empty($data) ) {
            if(!array_key_exists('modified_at', $data)) {
                $data['modified_at'] = date("Y-m-d H:i:s");
            }
            $update = $this->db->update('expenses', $data, $where );
            //return $update?true:false;
            if($this->db->affected_rows() == '1') return TRUE;
            else return FALSE;
        } else {
            return FALSE;
        }
    }
    
    /*
     * Delete expenses data
     */
    public function delete($where) {
        if( !empty($where) ) {
            $delete = $this->db->delete('expenses', $where );
            //return $delete?true:false;
            if($this->db->affected_rows() == '1') return TRUE;
            else return FALSE;
        } else {
            return FALSE;
        }
    }
}
?>