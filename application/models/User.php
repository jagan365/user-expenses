<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        //load database library
        $this->load->database();
    }

    /*
     * check login data
     */
    public function login($email,$password)
    {
        $q  = $this->db->select('password,id,role,name')->from('users')->where('email',$email)->get()->row();
       
        if($q == ""){
            return array('status' => 204,'message' => 'User email does not exist.');
        } else {
            $hashed_password = $q->password;
            $id              = $q->id;
            $user_role       = $q->role;
            $user_name       = $q->name;
            //echo $hashed_password ." ".$password;
            //exit;
            //if (password_verify($password, $hashed_password))) {
            if (hash_equals($hashed_password, crypt($password, $hashed_password))) {

                $last_login = date('Y-m-d H:i:s');
                $token = md5(uniqid($hashed_password, true));//crypt(substr( md5(rand()), 0, 10));
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                //$this->db->where('id',$id)->update('users',array('last_login' => $last_login));
                $this->db->where('id',$id)->update('users',array('last_login_at' => $last_login,'token' => $token,'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE){
                  $this->db->trans_rollback();
                  return array('status' => false,'message' => 'Internal server error.');
                } else {
                  $this->db->trans_commit();
                  return array('status' => true,'message' => 'Successfully login.','id' => $id, 'name' => $user_name, 'role' => $user_role, 'token' => $token );
                }

            } else {
               return array('status' => false,'message' => 'Wrong password.');
            }
        }
    }

    /*
     * check logout data
     */
    public function logout()
    {
        //$user_id  = $this->input->get_request_header('user_id', TRUE);
        $token     = $this->input->get_request_header('token', TRUE);
        $this->db->where('token',$token)->update('users',array('token' => NULL));

        //if ( $this->db->affected_rows() == '1' ) {return TRUE;}
        //else {return FALSE;}

        if($this->db->affected_rows() == '1') return TRUE;

        else  return FALSE;
        
    }

    /*
     * authenticate user 
     */
    public function auth()
    {
        //$users_id  = $this->input->get_request_header('user_id', TRUE);
        $token = $this->input->get_request_header('token', TRUE);
        if($token == ""){
            return array('status' => FALSE,'message' => 'Unauthorized.');
        } 
        $q  = $this->db->select('expired_at')->from('users')->where('token',$token)->get()->row();
        if($q == ""){
            return array('status' => FALSE,'message' => 'Unauthorized.');
        } else {
            if($q->expired_at < date('Y-m-d H:i:s')){
                return array('status' => FALSE,'message' => 'Your session has been expired.');
            } else {
                $modified_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('token',$token)->update('users',array('expired_at' => $expired_at,'modified_at' => $modified_at));
                $a = $this->db->select('id,role,permission_level')->from('users')->where('token',$token)->get()->row();
                return array('status' => 200,'message' => 'Authorized.','id' => $a->id,'role' => $a->role, 'permission_level'=>$a->permission_level);
            }
        }
    }

    public function permission_level($role)
    {
        $this->db->select('permission_level');
        $this->db->where('name', $role);
        $query = $this->db->get('roles');
        return $query->row_array();
    }

    public function email_exists($email)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('users');
        if( $query->num_rows() > 0 ){ return TRUE; } else { return FALSE; }
    }

    /*
     * Fetch user data
     */
    public function getRows($where){
        $this->db->select('id,name,email,role');
        if(!empty($where)){
            $this->db->where($where);
            $query = $this->db->get('users');
            return $query->result_array();
        }else{
            $query = $this->db->get('users');
            return $query->result_array();
        }
    }
    
    /*
     * Insert user data
     */
    public function insert($data = array()) {
        if(!array_key_exists('created_at', $data)){
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists('modified_at', $data)){
            $data['modified_at'] = date("Y-m-d H:i:s");
        }
        
        $insert = $this->db->insert('users', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return FALSE;
        }
    }
    
    /*
     * Update user data
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)){
            if(!array_key_exists('modified_at', $data)){
                $data['modified_at'] = date("Y-m-d H:i:s");
            }
            $update = $this->db->update('users', $data, array('id'=>$id));
            //return $update?true:false;
            if($this->db->affected_rows() == '1') return TRUE;
            else return FALSE;
        }else{
            return false;
        }
    }
    
    /*
     * Delete user data
     */
    public function delete($id){
        $delete = $this->db->delete('users',array('id'=>$id));
        //return $delete?true:false;
        if($this->db->affected_rows() == '1') return TRUE;
        else return FALSE;
    }

}
?>