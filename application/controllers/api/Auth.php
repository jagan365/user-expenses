<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class auth extends REST_Controller {

	public function __construct() { 
        parent::__construct();
		
		//load user model
        $this->load->model('user');
    }

    public function register_post()
    {
    	$userData = array();
		$userData['name'] = $this->post('name');
		$userData['email'] = $this->post('email');
		$userData['password'] = $this->post('password');
		$userData['role'] = "user";
		$userData['permission_level'] = 1;

		if(!empty($userData['name']) && !empty($userData['email']) && !empty($userData['password']) ) {
			if (filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
				$userData['password'] = password_hash($this->post('password'), PASSWORD_DEFAULT);
				$duplicate = $this->user->email_exists($userData['email']);

				if($duplicate) {
	            	$this->response(array('status' => FALSE,'message' => 'user email already exist.'));
	        	} else {
	        		//insert user data
					$insert = $this->user->insert($userData);				
					//check if the user data inserted
					if($insert){
						//set the response and exit
						$this->response([
							'status' => TRUE,
							'message' => 'User has been added successfully.'
						], REST_Controller::HTTP_OK);
					}else{
						//set the response and exit
						$this->response([
							'status' => False,
							'message' => 'Some problems occurred, please try again.'
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				}
			} else {
	            $this->response([
	                    'status' => FALSE,
	                    'message' => $userData['email'].' is not a valid email address.'
	            ], REST_Controller::HTTP_BAD_REQUEST);
	        }
        } else {
			//set the response and exit
			//BAD_REQUEST (400) being the HTTP response code
            $this->response([
				'status' => False,
				'message' => 'Provide complete user information to create.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
    }

	public function login_post()
	{		        
        $email = $this->post('email');
        $password = $this->post('password');
        //$role = $this->post('role');

	    if( !empty($email) && !empty($password) ){ 
	    	if (filter_var($email, FILTER_VALIDATE_EMAIL)) {   	
		        $validate = $this->user->login($email,$password);

		        if($validate['status'] === true) {

		        	$this->response( $validate, REST_Controller::HTTP_OK );

		        } else {
		        	$this->response([
						'status' => False,
						'message' => $validate['message']
					], REST_Controller::HTTP_BAD_REQUEST);
		        }				
			} else {
	            $this->response([
	                    'status' => FALSE,
	                    'message' => $user_email.' is not a valid email address.'
	            ], REST_Controller::HTTP_BAD_REQUEST);
	        }
		}else{
			//set the response and exit
			//BAD_REQUEST (400) being the HTTP response code
	        $this->response([
				'status' => False,
				'message' => 'Provide complete user information to create.'
			], REST_Controller::HTTP_BAD_REQUEST);
		}
	}
	
	public function logout_get()
	{	
		$validate = $this->user->auth();
		if($validate['status'] == 200){	
	        $response = $this->user->logout();

	        if($response) {
	        	$this->response([
					'status' => TRUE,
					'message' => 'User logout successfully.'
				], REST_Controller::HTTP_OK);
	        } else {
	        	$this->response([
					'status' => False,
					'message' => 'Unauthorized.'
				], REST_Controller::HTTP_BAD_REQUEST);
	        }
			$this->response($response);
		} else {
			$this->response([
				'status' => False,
				'message' => $validate['message']
			], REST_Controller::HTTP_BAD_REQUEST);
        }
	}	
}

?>