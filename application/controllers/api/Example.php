<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

//include Rest Controller library
require APPPATH . '/libraries/REST_Controller.php';

class example extends REST_Controller {

    public function __construct() { 
        parent::__construct();
		
		//load user model
        $this->load->model('user');
        $this->load->model('expenses');
    }
	
	/*public function user_get($id = 0) {
		//returns all rows if the id parameter doesn't exist,
		//otherwise single row will be returned
		$validate = $this->user->auth();
		if($validate['status'] == 200){
			if($id) {
				if($id == 'all') {
					if($validate['role'] == 'user'){
						//set the response and exit
						$this->response([
							'status' => FALSE,
							'message' => 'Permission denied.'
						], REST_Controller::HTTP_BAD_REQUEST);
					} else {
						if($validate['role'] == 'admin') $get_users = '2' ;
						else $get_users = '1' ;
						$users = $this->user->getRows($id,$get_users);
					}
				} else {
					if($validate['role'] == 'user' && $validate['id'] != $id){
						//set the response and exit
						$this->response([
							'status' => FALSE,
							'message' => 'Permission denied.'
						], REST_Controller::HTTP_BAD_REQUEST);
					} else {
						if($validate['role'] == 'admin') $get_users = '2' ;
						else $get_users = '1' ;
						$users = $this->user->getRows($id,$get_users);
					}
				}
			} else {
				$id = $validate['id'];
				$users = $this->user->getRows($id);
			}

			//check if the user data exists
			if(!empty($users)){
				//set the response and exit
				//OK (200) being the HTTP response code
				$this->response($users, REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				//NOT_FOUND (404) being the HTTP response code
				$this->response([
					'status' => FALSE,
					'message' => 'No user were found.'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response($validate);
        }
	}*/

	public function user_get($id = "") {
		//returns all rows if the id parameter doesn't exist,
		//otherwise single row will be returned
		$validate = $this->user->auth();
		if($validate['status'] == 200){
			if($id) {
				if($validate['role'] == 'admin') {
					if($id == 'all') $where = 'permission_level < 3';
					else $where = 'id ='.$id.' AND permission_level < 3';

					$users = $this->user->getRows($where);
				}
				else if($validate['role'] == 'manager') {
					if($id == 'all') $where = 'permission_level < 2';
					else $where = 'id ='.$id.' AND permission_level < 2';

					$users = $this->user->getRows($where);
				}
				else {
					$this->response([
						'status' => FALSE,
						'message' => 'Permission denied.'
					], REST_Controller::HTTP_BAD_REQUEST);
				}				
			} else {
				$where = 'id ='.$validate['id'];
				//$id = $validate['id'];
				$users = $this->user->getRows($where);
			}

			//check if the user data exists
			if(!empty($users)){
				//set the response and exit
				//OK (200) being the HTTP response code
				$this->response($users, REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				//NOT_FOUND (404) being the HTTP response code
				$this->response([
					'status' => FALSE,
					'message' => 'No user were found.'
				], REST_Controller::HTTP_NOT_FOUND);
			}

		} else {
			$this->response($validate);
        }
	}
	
	public function user_post() {
		$validate = $this->user->auth();
		if($validate['status'] == 200){
			if($validate['role'] != 'user'){
				$userData = array();
				$userData['name'] = $this->post('name');
				$userData['email'] = $this->post('email');
				$userData['password'] = $this->post('password');
				$userData['role'] = $this->post('role');
				if(!empty($userData['name']) && !empty($userData['email']) && !empty($userData['password']) && !empty($userData['role'])){
					if (filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
						$userData['password'] = password_hash($this->post('password'), PASSWORD_DEFAULT);
						$duplicate = $this->user->email_exists($userData['email']);

						if($duplicate) {
			            	$this->response(array('status' => FALSE,'message' => 'user email already exist.'));
			        	} else {

			        		$level = $this->user->permission_level($userData['role']);
			        		$userData['permission_level'] = $level['permission_level'];
							//insert user data
							$insert = $this->user->insert($userData);
							
							//check if the user data inserted
							if($insert){
								//set the response and exit
								$this->response([
									'status' => TRUE,
									'message' => 'User has been added successfully.',
									'id' => $insert
								], REST_Controller::HTTP_OK);
							}else{
								//set the response and exit
								$this->response([
									'status' => FALSE,
									'message' => 'Some problems occurred, please try again.'
								], REST_Controller::HTTP_BAD_REQUEST);
							}
						}
					} else {
						$this->response([
	                    	'status' => FALSE,
	                    	'message' => $userData['email'].' is not a valid email address.'
	            		], REST_Controller::HTTP_BAD_REQUEST);
					}
		        }else{
					//set the response and exit
					//BAD_REQUEST (400) being the HTTP response code
		            $this->response([
		            	'status' => FALSE,
		            	'message' => 'Provide complete user information to update.'
		            ], REST_Controller::HTTP_BAD_REQUEST);
				}
			} else {
				//set the response and exit
				$this->response([
					'status' => FALSE,
					'message' => 'Permission denied.'
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		} else {
			$this->response($validate);
    	}
	}
	
	public function user_put($user_id = "") {
		if($user_id) {
			$validate = $this->user->auth();
			if($validate['status'] == 200){
				
				if($validate['role'] != 'user') {
					$userData = array();
					$id = $user_id;
					$userData['name'] = $this->put('name');
					//$userData['email'] = $this->put('email');
					//$userData['password'] = $this->put('password');
					$userData['role'] = $this->put('role');

					if(!empty($userData['role'])) {
						$level = $this->user->permission_level($userData['role']);
			        	$userData['permission_level'] = $level['permission_level'];
					}

					foreach($userData as $key => $value)
					{
					    if(is_null($value) || $value == '')
					    unset($userData[$key]);
					}

					if(!empty($userData) ){
						//$userData['password'] = password_hash($this->put('password'), PASSWORD_DEFAULT);
						//$duplicate = $this->user->email_exists($userData['email']);
						//if($duplicate) {
			            	//$this->response(array('status' => FALSE,'message' => 'user email already exist.'));
			        	//} else {
						//update user data
						$update = $this->user->update($userData, $id);
						
						//check if the user data updated
						if($update){
							//set the response and exit
							$this->response([
								'status' => TRUE,
								'message' => 'User has been updated successfully.'
							], REST_Controller::HTTP_OK);
						}else{
							//set the response and exit
							$this->response([
								'status' => FALSE,
								'message' => 'Some problems occurred, please try again or The ID does not exist.'
							], REST_Controller::HTTP_BAD_REQUEST);
						}
						//}
			        }else{
						//set the response and exit
						//$this->response(['id'=>$id,'name'=>$userData['name'],'email'=>$userData['email'], 'password'=>$userData['password'], 'role'=>$userData['role']], REST_Controller::HTTP_BAD_REQUEST);
			            $this->response([
			            	'status' => FALSE,
			            	'message' => 'Provide complete user information to create.'
			            ], REST_Controller::HTTP_BAD_REQUEST);
					}
				} else {
					//set the response and exit
					$this->response([
						'status' => FALSE,
						'message' => 'Permission denied.'
					], REST_Controller::HTTP_BAD_REQUEST);
				}
			} else {
				$this->response($validate);
	    	}
	    } else {
			//set the response and exit
			$this->response([
				'status' => FALSE,
				'message' => 'No user is found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}
	
	public function user_delete($id) {
        //check whether post id is not empty
        if($id){
	       	$validate = $this->user->auth();
			if($validate['status'] == 200){

				//$where = 'id ='$id;
				//$users = $this->user->getRows($where);

				if($validate['role'] != 'user' && $id != $validate['id']){
		            //delete post
		            $delete = $this->user->delete($id);
		            
		            if($delete){
		                //set the response and exit
						$this->response([
							'status' => TRUE,
							'message' => 'User has been removed successfully.'
						], REST_Controller::HTTP_OK);
		            }else {
						//set the response and exit
						$this->response([
							'status' => FALSE,
							'message' => 'Some problems occurred, please try again or The ID does not exist.'
						], REST_Controller::HTTP_BAD_REQUEST);
					}
				} else {
					//set the response and exit
					$this->response([
						'status' => FALSE,
						'message' => 'Permission denied.'
					], REST_Controller::HTTP_BAD_REQUEST);
				}
	        } else {
				$this->response($validate);
        	}
        }else{
			//set the response and exit
			$this->response([
				'status' => FALSE,
				'message' => 'No user were found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
    }

    public function expenses_get($id = 0) {
		//returns all rows if the id parameter doesn't exist,
		//otherwise single row will be returned
		$validate = $this->user->auth();
		if($validate['status'] == 200){
		    $user_id = $validate['id'];
		    if( $validate['role'] == 'admin'){
		    	if(isset($_GET['user_id']) && !empty($_GET['user_id'])) $user_id = $_GET['user_id'];
		    }

		    function valid_date( $str )
			{
				$str = str_replace('/', '-', $str);
			    $stamp = strtotime( $str );
			      
			    if (!is_numeric($stamp)) 
			    { 
			        return FALSE;
			    } 
			      
				$day   = date( 'd', $stamp );
				$month = date( 'm', $stamp );
				$year  = date( 'Y', $stamp );

				if (checkdate($month, $day, $year)) 
				{
				return $year.'-'.$month.'-'.$day;
				}

				return FALSE;
			}

		    $where = "1=1";
		    if($id) $where .= ' AND id='.$id ;
		    if($user_id) $where .= ' AND user_id=' .$user_id;

		    if(isset($_GET['from_date'])  && !empty($_GET['from_date'])) {
		    	$from_date = NULL;
		    	$from_date = valid_date( $_GET['from_date'] );
		    	if($from_date === FALSE) {
			    	$this->response([
						'status' => FALSE,
						'message' => $_GET['from_date'].'not a valid date.'
					], REST_Controller::HTTP_BAD_REQUEST);
			    }
			    $where .= ' AND date >="'.$from_date.'"';
		    }
		    if(isset($_GET['to_date']) && !empty($_GET['to_date'])) {
		    	$to_date = NULL;
		    	$to_date = valid_date( $_GET['to_date'] );
		    	if($to_date === FALSE) {
			    	$this->response([
						'status' => FALSE,
						'message' => $_GET['to_date'].'not a valid date.'
					], REST_Controller::HTTP_BAD_REQUEST);
			    }
			    $where .= ' AND date <="'.$to_date.'"';
		    }
		    if(!empty($from_date) && !empty($to_date)){
		    	if(strtotime($to_date) < strtotime($from_date)) {
			    	$this->response([
						'status' => FALSE,
						'message' => 'from-date '.$from_date.' should be less than or equals to to-date '.$to_date
					], REST_Controller::HTTP_BAD_REQUEST);
			    }
		    }
		    if(isset($_GET['limit']) && !empty($_GET['limit'])) $where .= ' LIMIT '. $_GET['limit'];
		    if(isset($_GET['from']) && isset($_GET['to']) && !empty($_GET['from']) && !empty($_GET['to'])) $where .= ' LIMIT '.$_GET['from'].','.$_GET['to'];

			$expenses = $this->expenses->getRows($where);
			
			//check if the expenses data exists
			if(!empty($expenses)){
				//set the response and exit
				//OK (200) being the HTTP response code
				$this->response($expenses, REST_Controller::HTTP_OK);
			}else{
				//set the response and exit
				//NOT_FOUND (404) being the HTTP response code
				$this->response([
					'status' => FALSE,
					'message' => 'No expenses are found.'
				], REST_Controller::HTTP_NOT_FOUND);
			}
		} else {
			$this->response($validate);
        }
	}
	
	public function expenses_post() {
		$validate = $this->user->auth();
		if($validate['status'] == 200){
			$expensesData = array();
			$expensesData['description'] = $this->post('description');
			$expensesData['date'] = $this->post('date');
			$expensesData['time'] = $this->post('time');
			$expensesData['amount'] = $this->post('amount');
			$expensesData['comment'] = $this->post('comment');
			$expensesData['user_id'] = $validate['id'];
			if( $validate['role'] == 'admin'){
		    	//if(!empty($this->post('user_id'))) $expensesData['user_id'] = $this->post('user_id');
		    	if(isset($_GET['user_id'])) $expensesData['user_id'] = $_GET['user_id'];
		    }
			if(!empty($expensesData['description']) && !empty($expensesData['date']) && !empty($expensesData['time']) && !empty($expensesData['amount']) && !empty($expensesData['comment']) && !empty($expensesData['user_id']) ){
				//insert expenses data
				$insert = $this->expenses->insert($expensesData);
				
				//check if the expenses data inserted
				if($insert){
					//set the response and exit
					$this->response([
						'status' => TRUE,
						'message' => 'expense has been added successfully.',
						'id' => $insert
					], REST_Controller::HTTP_OK);
				}else {
					$this->response([
							'status' => FALSE,
							'message' => 'Some problems occurred, please try again.'
						], REST_Controller::HTTP_BAD_REQUEST);
				}
	        }else{
				//set the response and exit
				//BAD_REQUEST (400) being the HTTP response code
	            $this->response([
		            	'status' => FALSE,
		            	'message' => 'Provide complete expense information to create.'
		            ], REST_Controller::HTTP_BAD_REQUEST);
			}
		} else {
			$this->response($validate);
    	}
	}
	
	public function expenses_put($id) {
		if($id){
			$validate = $this->user->auth();
			if($validate['status'] == 200){
				$expensesData = array();
				//$id = $this->put('id');
				$expensesData['description'] = $this->put('description');
				$expensesData['date'] = $this->put('date');
				$expensesData['time'] = $this->put('time');
				$expensesData['amount'] = $this->put('amount');
				$expensesData['comment'] = $this->put('comment');
				$user_id = $validate['id'];

				if( $validate['role'] == 'admin'){
			    	$where = array();
			    	$where['id'] = $id;
			    } else {
			    	$where = array();
			    	$where['id'] = $id;
			    	$where['user_id'] = $user_id;
			    }

			    foreach($expensesData as $key => $value)
				{
				    if(is_null($value) || $value == '')
				    unset($expensesData[$key]);
				}

				if( !empty($expensesData) ){
					//update expenses data
					$update = $this->expenses->update($expensesData, $where);
					
					//check if the expenses data updated
					if($update){
						//set the response and exit
						$this->response([
							'status' => TRUE,
							'message' => 'expenses has been updated successfully.'
						], REST_Controller::HTTP_OK);
					}else{
						//set the response and exit
						$this->response([
							'status' => FALSE,
							'message' => 'Some problems occurred, please try again or the id provided does not exist for corresponding user.'
						], REST_Controller::HTTP_BAD_REQUEST);
					}		
		        }else{
					//set the response and exit
		            $this->response([
		            	'status' => FALSE,
		            	'message' => 'Provide complete expense information to update.'
		            ], REST_Controller::HTTP_BAD_REQUEST);
				}
			} else {
				$this->response($validate);
        	}
		} else {
			//set the response and exit
			$this->response([
				'status' => FALSE,
				'message' => 'No expenses is found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
	}
	
	public function expenses_delete($id) {
        //check whether post id is not empty
        if($id){
        	$validate = $this->user->auth();
			if($validate['status'] == 200){
				$user_id = $validate['id'];

				if( $validate['role'] == 'admin'){
			    	$where = array();
			    	$where['id'] = $id;
			    } else {
			    	$where = array();
			    	$where['id'] = $id;
			    	$where['user_id'] = $user_id;
			    }

	            //delete post
	            $delete = $this->expenses->delete($where);
	            
	            if($delete){
	                //set the response and exit
					$this->response([
						'status' => TRUE,
						'message' => 'expenses has been removed successfully.'
					], REST_Controller::HTTP_OK);
	            }else{
	                //set the response and exit
					$this->response([
						'status' => FALSE,
						'message' => 'Some problems occurred, please try again or the id provided does not exist for corresponding user.'
					], REST_Controller::HTTP_BAD_REQUEST);
	            }
	        } else {
				$this->response($validate);
        	}
        }else{
			//set the response and exit
			$this->response([
				'status' => FALSE,
				'message' => 'No expenses is found.'
			], REST_Controller::HTTP_NOT_FOUND);
		}
    } 

    public function expenses_perweek_get($id = 0) {
		//returns all rows if the id parameter doesn't exist,
		//otherwise single row will be returned
		if(isset($_GET['from_date'])  && !empty($_GET['from_date']) && isset($_GET['to_date'])  && !empty($_GET['to_date'])) {
			$validate = $this->user->auth();
			if($validate['status'] == 200){
			    $user_id = $validate['id'];
			    if( $validate['role'] == 'admin'){
			    	if(isset($_GET['user_id']) && !empty($_GET['user_id'])) $user_id = $_GET['user_id'];
			    }

			    function valid_date( $str )
				{
					$str = str_replace('/', '-', $str);
				    $stamp = strtotime( $str );
				      
				    if (!is_numeric($stamp)) 
				    { 
				        return FALSE;
				    } 
				      
					$day   = date( 'd', $stamp );
					$month = date( 'm', $stamp );
					$year  = date( 'Y', $stamp );

					if (checkdate($month, $day, $year)) 
					{
					return $year.'-'.$month.'-'.$day;
					}

					return FALSE;
				}

			    $where = "1=1";
			    if($id) $where .= ' AND id='.$id ;
			    if($user_id) $where .= ' AND user_id=' .$user_id;

			    if(isset($_GET['from_date'])  && !empty($_GET['from_date'])) {
			    	$from_date = NULL;
			    	$from_date = valid_date( $_GET['from_date'] );
			    	if($from_date === FALSE) {
				    	$this->response([
							'status' => FALSE,
							'message' => $_GET['from_date'].'not a valid date.'
						], REST_Controller::HTTP_BAD_REQUEST);
				    }
				    $where .= ' AND date >="'.$from_date.'"';
			    }
			    if(isset($_GET['to_date']) && !empty($_GET['to_date'])) {
			    	$to_date = NULL;
			    	$to_date = valid_date( $_GET['to_date'] );
			    	if($to_date === FALSE) {
				    	$this->response([
							'status' => FALSE,
							'message' => $_GET['to_date'].'not a valid date.'
						], REST_Controller::HTTP_BAD_REQUEST);
				    }
				    $where .= ' AND date <="'.$to_date.'"';
			    }
			    if(!empty($from_date) && !empty($to_date)){
			    	if(strtotime($to_date) < strtotime($from_date)) {
				    	$this->response([
							'status' => FALSE,
							'message' => 'from-date '.$from_date.' should be less than or equals to to-date '.$to_date
						], REST_Controller::HTTP_BAD_REQUEST);
				    }
				    $days = "";
				    //$days = date_diff(date('Y-m-d',strtotime($to_date)),date('Y-m-d',strtotime($from_date)));
			    }
			    if(isset($_GET['limit']) && !empty($_GET['limit'])) $where .= ' LIMIT '. $_GET['limit'];
			    if(isset($_GET['from']) && isset($_GET['to']) && !empty($_GET['from']) && !empty($_GET['to'])) $where .= ' LIMIT '.$_GET['from'].','.$_GET['to'];

			    $diff = strtotime($to_date) - strtotime($from_date);
			    $days = ( $diff+86400 )/(60 * 60 * 24) ; //date('Y-m-d',strtotime($to_date));

				$sum_avg = $this->expenses->getSumAvg($where, $days);

				//$variable =json_decode($sum_avg);

				foreach ($sum_avg as $key => $value) {
					if($key == 'sum') $sum = $value;
					if($key == 'average') $avg = $value;
				}

				$expenses = $this->expenses->getRows($where);
				
				//check if the expenses data exists
				if(!empty($expenses) && !empty($sum_avg)){
					//set the response and exit
					//OK (200) being the HTTP response code
					$this->response([
						'status' => True,
						'sum' => $sum,
						'days'=> $days,
						'averege per day' => $avg,
						'data' => $expenses
						], REST_Controller::HTTP_OK);
				}else{
					//set the response and exit
					//NOT_FOUND (404) being the HTTP response code
					$this->response([
						'status' => FALSE,
						'message' => 'No expenses are found.'
					], REST_Controller::HTTP_NOT_FOUND);
				}
			} else {
				$this->response($validate);
	        }
	    } else {
	    	$this->response([
				'status' => FALSE,
				'message' => 'Please provide from-date and to-date.'
			], REST_Controller::HTTP_BAD_REQUEST);
	    }
	}

    public function expenses_filter_get() {
    	date_default_timezone_set("Asia/Kolkata");

    	function valid_date( $str )
		{
			$str = str_replace('/', '-', $str);
		    $stamp = strtotime( $str );
		      
		    if (!is_numeric($stamp)) 
		    { 
		        return FALSE;
		    } 
		      
			$day   = date( 'd', $stamp );
			$month = date( 'm', $stamp );
			$year  = date( 'Y', $stamp );

			if (checkdate($day, $month, $year)) 
			{
			return $year.'-'.$month.'-'.$day;
			}

			return FALSE;
		}

    	$validate = $this->user->auth();
		if($validate['status'] == 200){

			if(isset($_GET['from_date'])) $from_date = valid_date( $_GET['from_date'] );
		    if(isset($_GET['to_date'])) $to_date = valid_date( $_GET['to_date'] );
		    else $to_date = date("Y-m-d");

		    if($from_date === FALSE) {
		    	$this->response([
						'status' => FALSE,
						'message' => $from_date.'not a valid date.'
					], REST_Controller::HTTP_BAD_REQUEST);
		    }

		    if($to_date === FALSE) {
		    	$this->response([
						'status' => FALSE,
						'message' => $to_date.'not a valid date.'
					], REST_Controller::HTTP_BAD_REQUEST);
		    }

		    if(strtotime($to_date) < strtotime($from_date)) {
		    	$this->response([
						'status' => FALSE,
						'message' => 'from date '.$from_date.' should be less than to date '.$to_date
					], REST_Controller::HTTP_BAD_REQUEST);
		    }

		    if( !empty($from_date) ) { 

		    	//$from_date = valid_date( $_GET['from_date'] );
				//$to_date = valid_date( $_GET['to_date'] );

			    $user_id = $validate['id'];

			    $filter = 'date >="' .$from_date. '" AND date <="' .$to_date. '" AND user_id = ' .$user_id;

			    /*$this->response([
						'status' => FALSE,
						'message' => 'No expenses are found.',
						'from date' => $from_date,
						'to date' => $to_date
					], REST_Controller::HTTP_OK);*/
			    
				$expenses = $this->expenses->getFilterRows($filter);
				
				//check if the expenses data exists
				if(!empty($expenses)){
					//set the response and exit
					//OK (200) being the HTTP response code
					$this->response($expenses, REST_Controller::HTTP_OK);
				}else{
					//set the response and exit
					//NOT_FOUND (404) being the HTTP response code
					$this->response([
						'status' => FALSE,
						'message' => 'No expenses are found.'
					], REST_Controller::HTTP_NOT_FOUND);
				}
			} else{
				$this->response([
	            	'status' => FALSE,
	            	'message' => 'Provide date parameters to filter.'
	            ], REST_Controller::HTTP_BAD_REQUEST);
			}
		} else {
			$this->response($validate);
        }
	} 
}

?>
