<html>
<head>
<title>home</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
	<div class='message'></div>
	
	<div id="login">
		<div id="main">
		<div id="form">
			<h2>Login Form</h2>
			<hr/>
			<div id="login_err" class='error_msg'></div>
			<label>Email :</label>
			<input type="email" name="email" id="login_email" placeholder="email-id" required autocomplete="off"/><br /><br />
			<label>Password :</label>
			<input type="password" name="password" id="login_password" placeholder="**********" required autocomplete="off"/><br/><br />

			<input type="radio" id="contactChoice1" name="role" value="user" checked>
    		<label for="contactChoice1">User</label>
		    <input type="radio" id="contactChoice2" name="role" value="manager">
		    <label for="contactChoice2">Manager</label>
		    <input type="radio" id="contactChoice3" name="role" value="admin">
		    <label for="contactChoice3">Admin</label><br/><br />

			<button type="submit" onClick="LoginValidation();">Login</button><br />
			<a href="#" onClick = "$('#login').hide(); $('#signup').show()">To SignUp Click Here</a>
		</div>
	</div>
	</div>
	<div id="signup">
		<div id="main">
		<div id="form">
			<h2>Registration Form</h2>
			<hr/>
			<div id="reg_err" class='error_msg'></div>
			<label>Name :</label>
			<input type="text" name="name" id="reg_name" placeholder="username" required autocomplete="off"/><br /><br />
			<label>Email :</label>
			<input type="email" name="email" id="reg_email" placeholder="email-id" required autocomplete="off"/><br /><br />
			<label>Password :</label>
			<input type="password" name="password" id="reg_password" placeholder="**********"/ required autocomplete="off"><br/><br />
			<button type="submit" onClick="signUpValidation();">Sign Up</button><br />
			<a href="#" onClick = "$('#signup').hide(); $('#login').show()">Login Click Here</a>
		</div>
	</div>
	</div>
<script>
	var login_token;
	function signUpValidation() {
        var name = document.getElementById("reg_name").value;
        var email = document.getElementById("reg_email").value;
        var pswd = document.getElementById("reg_password").value;
        var pswd_pattern = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{4,8}$/;
        var email_pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
        
        if(name != "" && email != "" && pswd != "") {

            if( email_pattern.test(email) ) {
	            if( pswd_pattern.test(pswd) ) {
	            	
	            		document.getElementById('reg_err').style.display = "none";
	            
	            		/*console.log(email);
		            	console.log(pswd);
		            	console.log(role);*/

		            	//signUp();    //Ajax call to server for signup varification
		            	var data = {
		            	"name":name,
			            "email": email,
			            "password": pswd,
			            };
			            $.ajax({
				            url: "http://104.236.111.96/index.php/api/auth/register", 
				            headers: { "api_key": "api_auth@2018" }        
				            type: "POST",
				            data: data,
				            success: function (response){
				                if (response["success"] == true){
				                    console.log(response);
				                    document.getElementById('reg_err').style.display = "block";
	            					document.getElementById('reg_err').innerHTML = "&#42;"+response["message"];
	            					//$("ul.tabs li:first").addClass("active").show();

				                }
				                else{
				                	document.getElementById('reg_err').style.display = "block";
	            					document.getElementById('reg_err').innerHTML = "&#42;"+response["message"];
				                }
				            },
				            error: function (jqXHR, textStatus, ex) {
				              console.log(textStatus + "," + ex + "," + jqXHR.responseText);
				            }
			            });
	            }
	            else{
	            	document.getElementById('reg_err').style.display = "block";
	            	document.getElementById('reg_err').innerHTML = "&#42;Please Select a password between 4-8 characters and contains atleast one numeric and a special character";
	            }
	        }
	        else{
	        	document.getElementById('reg_err').style.display = "block";
	        	document.getElementById('reg_err').innerHTML = "&#42;Please Enter Valid Email-ID";
			}	            
        }
        else{
        	document.getElementById('reg_err').style.display = "block";
            document.getElementById('reg_err').innerHTML = "&#42;Please Fill The Fields Above";
        }        
    }

    function LoginValidation() {
        var login_email = document.getElementById("login_email").value;
        var login_pswd = document.getElementById("login_password").value;
        var login_role = $("input[name='role']:checked").val();
        //var login_role = document.getElementById("login_role").value;
        var email_pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
        
        if(login_email == "" && login_pswd == "") {
            document.getElementById('login_err').style.display = "block";
            document.getElementById('login_err').innerHTML = "&#42;Please Fill The Fields Above";	            
        } else {    	            
	        if(email_pattern.test(login_email)) {

	        	/*console.log(login_email);
	            console.log(login_pswd);
	            console.log(login_role);

	            if(login_role == "user") {
	            	document.getElementById('forms').style.display = "none";
	            	document.getElementById('user').style.display = "block";
	            }
	            if(login_role == "manager") {
	            	document.getElementById('forms').style.display = "none";
	            	document.getElementById('manager').style.display = "block";
	            }
	            if(login_role == "admin") {
	            	document.getElementById('forms').style.display = "none";
	            	document.getElementById('admin').style.display = "block";
	            }*/

	            //login();    //Ajax call to server for signup varification
	            var data = {
		            "email": login_email,
		            "password": login_pswd,
		            "role": login_role
		            };
	            $.ajax({
		            url: "http://104.236.111.96/index.php/api/auth/login",         
		            type: 'POST',
		            data: data,
		            success: function (response){
		                if (response["status"] == 200){
		                    console.log(response);
		                    login_token = response.data["token"];
		                    /*document.getElementById('forms').style.display = "none";
	            			document.getElementById('user').style.display = "block";

	            			$.ajax({
					            url: "http://159.89.35.44/api/meals",         
					            type: 'GET',
					            success: function (response){
					                if (response){
					                    console.log(response);
										var obj = response;
										console.log(obj);
										for(var key in obj){

										$('#example tbody tr:last').after('<tr role="row"><td class="sorting_1">' + obj[key].id + '</td><td>' + obj[key].Meal + '</td><td>' + obj[key].date + '</td><td>' + obj[key].time + '</td><td>' + obj[key].calories + '</td><td><a data-id="row-' + obj.id + '" href="javascript:editRow(' + obj[key].id + ');" class="btn btn-default btn-sm">edit</a>&nbsp;<a href="javascript:removeRow(' + obj[key].id + ');" class="btn btn-default btn-sm">delete</a></td></tr>');		
										}						                    
					                }
					            },
					            error: function (jqXHR, textStatus, ex) {
					              console.log(textStatus + "," + ex + "," + jqXHR.responseText);
					            }
				            });*/
		                }
		                if(response["status"] != 200){
		                	document.getElementById('login_err').style.display = "block";
		        			document.getElementById('login_err').innerHTML = "&#42;"+response["message"];
		                }
		            },
		            error: function (jqXHR, textStatus, ex) {
		              console.log(textStatus + "," + ex + "," + jqXHR.responseText);
		              //document.getElementById('login_error').style.display = "block";
		        	  //document.getElementById('login_error').innerHTML = "&#42;" + jqXHR.responseText['error'];
		            }
	            });
	        } else {
		        document.getElementById('login_err').style.display = "block";
		        document.getElementById('login_err').innerHTML = "&#42;Please Enter Valid Email-ID";
	    	}	        
        }        
    }
    function logout(){
        $.ajax({
            url: "http://104.236.111.96/index.php/api/auth/logout?token="+login_token,         
            type: 'GET',
           
            success: function (response){
                if (response){
                    console.log(response);
                    alert("you logout successfully");
                    document.getElementById('user').style.display = "none";
                    document.getElementById('forms').style.display = "block";
			        $("ul.tabs li:last").addClass("active").show();
                }
            },
            error: function (jqXHR, textStatus, ex) {
              console.log(textStatus + "," + ex + "," + jqXHR.responseText);
            }
        });

    }
</script>	
</body>
</html>